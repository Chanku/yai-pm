#YAI-PM (Yet Another Init - Process Manager/Yet another process manager)

####What is YAI-PM?
It's designed to be a simplistic Process Manager for Yet Another Init. It can work with it's own files, or it can work with epoch's files. I plan to eventually support daemontools scripts in some capacity. 

####Installation
There are no installation instructions at this time. 

####Requirements
It requires the following:
- Python 3 (Python 3.4 is recomended)  

- An Already working init system 

##PFAQ (Possibly Frequently Asked Questions)

####Why Python?
Because I felt like it would be interesting to do. If anything it's a nice learning oppertunity. This also allows me to do a lot of development, and allows it to be 'open' to people whom might be interested in similar things. I also only really know enough of Python to do this, and not much of any other language.  

####Why MIT License?
Simple, I wanted to use a simple free and permissive license. 

####Is this ready?
Nope

####Is this worth it?/Should I use this?
Probably not. However feel free to do hatever

####Why have built-in compatability with Epoch's Files first?
Well it isn't exactly built in, it's actually a seperate module designed to allow it to parse the files, however otherwise it's entirely seperate from the actually manager, excluding development space. I did this because the way the files in here work is similar to Epoch's, and I am rather familiar with Epoch (but not with daemontools or runit). 
