#
# This is where the config file will be parsed and broken down for use by the main program. 
# As such this area is probably going to be one of the more complicated areas
# See the implimentation document for the documentation of the config and how it should work.
#

#
# This method is designed to parse a config's data after it is read. 
# This is incase the file's existance is temporary and based in time.
# Or if it is desigred to actually be able to keep a record of what it what at launch.
# parse_live can do that, but it is designed to parse the file and open it.
# This is not the default in YAI-PM but may be swapped in the code
#
def parse(data):
    print("Not yet implimented")

#
# This method is deisgned to parse a config's data while reading it.
# This works by reading it line by line and storing it in data. 
# While simpler than parse in implimentation, it may take more time, 
# especially if the file is large. If the file is lost then there may
# be issues running. However this is the default implimentation used
# by YAI-PM, it may be switched in the YAI-PM main code.
#
# Services here are stored in a dictionary, with the name as a key
#  and another dictionary storing the data of the service.
#  This allows there to be some dynamism in how this works.
#
# This returns a dictionary (services), where it has the keys of all of the standard values, 
#   and it returns a list (ordered_services) in which services are listed in the order of their
#   Service_Start_Priority item. If two have the same Service_Start_Priority item, then the later
#   one executes first. 
#
def parse_live(file_name):
    services = {} 
    ordered_services = []
    with open(file_name, 'r') as config_file:
        is_in_service = False
        service = {}
        service_name = ""
        for line in config_file:
            if not line.startswith("#"):
                if "#" in line:
                    line = line.split("#")[0]
                if "Service_Name" in line:
                    line2 = line.strip("{")
                    line2 = line.strip('\n')
                    service["name"] = line2.split("=")[1]
                    is_in_service = True
                    service_name = line2.split("=")[1]
                if is_in_service:
                    line2 = line.strip("}")
                    line2 = line.strip('\n')
                    if "Service_Stop_Command" in line:
                        service["stop_command"] = line2.split("=")[1]
                    elif "Service_Start_Command" in line:
                        service["start_command"] = line2.split("=")[1]
                    elif "Service_Start_Priority" in line:
                        service["start_priority"] = line2.split("=")[1]
                    elif "Service_Stop_Priority" in line:
                        service["stop_priority"] = line2.split("=")[1]
                    elif "Service_Description" in line:
                        service["description"] = line2.split("=")[1]
                    if "}" in line:
                        services[service_name] = service 
                        service = {}
                        service_name = ""
                        is_in_service = False

    for key in services:
        Start_Priority = int(services[key]['start_priority']])
        ordered_services.insert(Start_Priority, services[key])

    return services, ordered_services
