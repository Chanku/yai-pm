##YAI-PM Code Documentation 

####pid_of
#####Argument(s): String name
#####Return Value(s): bytes (Encoded String)
#####Description: This merely returns the PID(s) of a process. 
#####Usage: x = pid_of('some_program')

####handle_services(services_dict):
#####Arguments(s): Dictionary of Services
#####Return Values(s): Nothing
#####Description: This handles the actual Service Supervision/Management of the program
#####Usage: handle_services(service = {})
