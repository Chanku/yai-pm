#Documentation for the YAI-PM Files 

###Service_Name:

#####Usage Examples:
Service_Name="test 1"{ 'service stuff' }

###Service_Description:
Sets a service description for a service. This is optional. 

#####Usage Examples:
Service_Description="Nip nop"

###Service_Start_Priority:
Sets the start priority for a service. In an order of 0 to X, in which the lowest number is started first (1 before 2, 2 before 3, 50 before 100, ect.). This is required for the service. 

######Usage Examples:
Service_Start_Priority=0 

###Service_Start_Command:
Sets the command that is executed to start the service. This is required for the service.

######Usage Examples:
Service_Start_Command="/bin/mount -o remount, rw/"

###Service_Stop_Command:
Sets the command that is used to stop the service. This is required, however if there is none then you can use None.

#####Usage Examples:
Service_Stop_Command="/bin/mount -o remount, ro/"

###Service_Stop_Priority:
Sets the stop priority for a service. Works in the same way as Service_Start_Priorities. This is Required.

#####Usage Examples:
Service_Stop_Priority=0

###Misc. Stuff (Optional)
The following things are stuff that you do not need to exactly know, and can function without, but goes into more depth in how things work. These are items such as comments are an explanation of the use od double quotes. 

####Optional Stuff
Comments (#) - Things ignored by the parser. A line will only be read up to the comment sign, if it is at the start of a line, it is ignored completely.

####Mandatory Stuff
Strings ("") - Any and all Strings must be set by enclosing it in Quotes. This is mandatory for anything that uses a string as shown (Any statement that has "" in it's usage example requires Double Quotes to be used.

