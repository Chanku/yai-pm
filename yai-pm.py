#!/usr/bin/python
from sys import stdout
from time import sleep
from subprocess import Popen

EPOCH_CONFIG = "/etc/epoch/epoch.conf"

#
# This is what handles the services. It takes dictionary of the services.
# This allows better mixing of living_services and killed_services.
# The services_dict is just as a list of all services entered by the user
# It does not return anything.
#
def handle_services(services_dict):
    living_services = {} #This is a dictionary of the living services 
    killed_services = {} #This is a dictionary of the killed/error services services. 
    living_services = services_list
    while True:
        sleep(1)
        for key in services_dict:
            living_poll = living_services[key]['popen'].poll()
            services_poll = services_dict[key]['popen'].poll()
            if services_poll == 0 or services_poll:
                if living_poll == 0 or living_poll:
                    services_dict[key]['popen'] = Popen(services_dict[key]['start_command'].strip('"'), shell=True)
                    living_services[key] = services_dict[key]
                elif not living_poll:
                    try:
                        living_services[key]['popen'].terminate()
                    except OSError:
                        goofable = 1
                        del goofable
                    services_dict[key]['popen'] = Popen(services_dict[key]['start_command'].strip('"'), shell=True)
                    living_services[key] = services_dict[key]
            elif services_poll != 0 and not services_poll:
                if living_poll == 0 or living_poll:
                    living_services[key] = services_dict[key]

try:
    services = []
    f = open('/etc/yai-pm/yai-pm.conf', 'r')
    f.close()
    from parsing.standard_conf import parse_live
    start_stuff, ordered_start_stuff = parse_live('/etc/yai-pm/yai-pm.conf')
    for item in ordered_start_stuff:
        name = item["name"].strip('"')
        sys.stdout.write("Starting " + name + "...\r")
        try:
            c = Popen(item['start_command'].strip('"'), shell=True)
            c.terminate()
            exit_numb = c.wait()
            if exit_numb == 0 or exit_numb == -15:
                try:
                    sys.stdout.write("Starting " + item["description"].strip("") + ".... [SUCCESS]\n")
                except KeyError:
                    sys.stdout.write("Starting " + name + ".... [SUCCESS]\n")
                item["popen"] = popen(item["start_command"])
                services.append(item)
            if exit_numb != 0 and exit_numb != -15:
                sys.stdout.write("Starting " + name + ".... [FAILURE!]\n")
                # sys.stdout.write("writting error to log as /etc/yai-pm/failure-log\n")
                # f = open('/etc/yai-pm/failure-log', 'a')
                print(e)
                # f.close()
        except OSError as e:
            sys.stdout.write("Starting " + name + ".... [FAILURE!]\n")
            # sys.stdout.write("writting error to log as /etc/yai-pm/failure-log\n")
            # f = open('/etc/yai-pm/failure-log', 'a')
            print(e)
            # f.close()
    handle_services(start_stuff)
except:
    print("AN EXCEPTION OCCURED!")
